"use strict";

describe('test example', function() {

	describe('empty array', function() {

		// create empty array
		var a = [];

		it("has 0 for length", function () {
			expect(a.length).toBe(0);
		});

		it("is equal to a empty array", function () {
			expect(a).toEqual([]);
		});

		describe('something next', function () {})

	});

	describe('example 2', function() {});

});